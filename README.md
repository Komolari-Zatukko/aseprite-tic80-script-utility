# Aseprite-Tic80 Tool

[Aseprite](https://www.aseprite.org/) script for [TIC-80](https://tic80.com/) compatibility.

<img src="icon256x.png" width="256" height="256" alt="tool icon">

- TIC-80 ROM language support :  
```
+------------+-----------+---------+
| Language   | Supported | Planned |
+------------+-----------+---------+
| Fennel     |     -     |    -    |
| Janet      |     -     |    -    |
| JavaScript |     -     |    -    |
| Lua        |    yes    |   yes   |
| Moonscript |     -     |    -    |
| Python     |     -     |    -    |
| Ruby       |     -     |    -    |
| Scheme     |     -     |    -    |
| Squirell   |     -     |    -    |
| TIC binary |     -     |   yes   |
| WASM       |     -     |    -    |
| Wren       |     -     |    -    |
+------------+-----------+---------+
```
- Import options :
	- [x] Bits per pixel.
	- [x] Import Tiles.
	- [x] Import Sprite.
	- [x] Import Palette.
	- [ ] Import Cover.
	- [ ] Memory banks handling.
- Export options :
	- [x] Bits per pixel.
	- [x] Generate binary string data for animated sprites/tiles (clipboard copy button).
	- [ ] Make sure generated data works for metasprites.
	- [ ] Export Cover.
	- [ ] Export Palette.
	- [ ] Export VRAM Tilemap.
	- [ ] Export VRAM Spritemap.