local scriptName<const> = "Tic-80 Link"
local pluginKey<const> = "kZatukko/tic80interface"

local currentState = "default"
local stateOptions = {}
local template = {
	default = function( options )
		local title = "Select Action"
		local dlg = Dialog{ title = ("%s : %s"):format( scriptName, title ) }
		
		-- The importation tab
		dlg:tab{
			text = "IMPORT"
		}
			-- File importation
			:separator { text = "ROM file to scan" }
				:file{
					id = "tic80RomFile",
					title = scriptName,
					open = false,
					filetypes = {"lua"}
				}
			-- Scan options
			:separator { text = "Default scan options" }
				-- Scan for tiles ?
				:check{
					id = "scanTiles",
					text = "Scan for Tiles ?",
					selected = true
				}:newrow()
			
				-- Scan for sprites ?
				:check{
					id = "scanSprites",
					text = "Scan for Sprites ?",
					selected = true
				}:newrow()
			
				-- Scan for palette ?
				:check{
					id = "scanPalette",
					text = "Scan for palette ?",
					selected = true
				}:newrow()
			-- More options
			:separator{ text = "Advanced scan options" }
				-- Bits per pixels
				:label{	text = "Select Bits per pixels :"}
				:combobox{
					id = "importBPP",
					options = { 4, 2, 1 },
					option = 1
				}
			-- Confirm/Cancel
			:separator()
			:button{ id="scan", text="Start Scan" }
			:button{ id="cancel", text="Cancel" }
		
		-- The exportation tab
		dlg:tab{ text = "EXPORT" }
			
			:label{	text = "Select Bits per pixels :"}
				:combobox{
					id = "exportBPP",
					options = { 4, 2, 1 },
					option = 1
				}
			
			:separator()
			:button{ id="generateData", text="Generate Data" }
			:button{ id="cancel", text="Cancel" }
		
		dlg:endtabs()
		
		stateOptions = {}
		return dlg
	end,
	
	
	scanResult = function( options )
		local title = "Scan result"
		local dlg = Dialog{title = ("%s : %s"):format( scriptName, title )}
		
		-- Confirm/Cancel
		:button{ id="confirm", text="Confirm" }
		:button{ id="cancel", text="Cancel" }
		
		stateOptions = {}
		return dlg
	end,
	
	generatedData = function( options )
		local data = options.data
		local title = "Generated Data"
		local dlg = Dialog{title = ("%s : %s"):format( scriptName, title )}
		
		-- This button copies generated data to clipboard
		:button{
			text="Copy data to clipboard",
			onclick=function()
				io.popen("clip","w"):write(data):close()
			end
		}
		:newrow()
		
		-- This button prints generated data into console
		:button{
			text="Show data in console",
			onclick=function()
				print(data)
			end
		}
		:newrow()
		
		-- Continue/Cancel
		:button{ id="continue", text="Continue" }
		:button{ id="cancel", text="Close" }
		
		stateOptions = {}
		return dlg
	end
}


-- Convert a string data to a data table
local function parseStringData( line, length )
	local data = {}
	local bitsIn = #line / length
	for i=1, length do
		local b = line:sub( (i-1) * bitsIn + 1, i * bitsIn )
		data[i] = tonumber( b, 16 )
	end
	return data
end


-- Convert a binary data string to an Aseprite Color Palette Object
local function dataToPalette( line )
	local pal = Palette(16)
	local cont = line:sub(8,-1)
	
	for j=0, 15 do
		local segment = cont:sub( j*6+1, (j+1)*6 )
		local r, g, b
		r = tonumber( segment:sub(1,2), 16 )
		g = tonumber( segment:sub(3,4), 16 )
		b = tonumber( segment:sub(5,6), 16 )
		pal:setColor( j, Color{ r=r, g=g, b=b, a=0xFF } )
	end
	
	return pal
end


-- Draw a data chunk to given image
local function drawDataChunkTo( img, data, bpp, strech, x, y )
	for i=1, #data do
		-- Get pos in data
		local c = ((i-1) % 8) * strech
		local r = (i-1) // 8
		-- Get nibble from data
		local nibble = data[i]
		
		for j=1, strech do
			-- Get the last bits from nibble and right shift 
			-- the remaining bits (by 1, 2 or 4 depending on bpp)
			local bits = nibble & (2^bpp - 1)
			nibble = nibble >> bpp
			
			-- Draw pixels using colors found in nibbles
			img:drawPixel( x + c + j-1, y + r, Color{ index = bits } )
		end
	end
end



do 	::ScriptStart::
	
	local dlg = template[ currentState ]( stateOptions ):show()
	local data = dlg.data
	
	if data.continue then
		currentState = "default"
		stateOptions = {}
		goto ScriptStart
	end
	
	if data.scan then
		
		local scanTiles = data.scanTiles
		local scanSprites = data.scanSprites
		local scanPalette = data.scanPalette
		
		local tiles = {}
		local sprites = {}
		local palettes = {}
		
		local file<close> = io.open( data.tic80RomFile, "r" )
		if not file then
			app.alert"No file selected !"
			goto ScriptStart
		end
		
		do
			local line
			local isTilesChunk = false
			local isSpritesChunk = false
			local isPaletteChunk = false
			::ScanNextLine::
			
			-- Read next line, if no there is no next line, skip checks
			line = file:read"*l"
			if not line then goto FileScanEnd end
			
			-- If the current line is not a full-line comment, look to the next one
			if line:find"-- " ~= 1 then goto ScanNextLine end
			
			if scanTiles then
				if line == "-- <TILES>" then isTilesChunk = true; goto ScanNextLine end
				if line == "-- </TILES>" then isTilesChunk = false; goto ScanNextLine end
				
				if isTilesChunk then
					-- Check if we reached the tiles chunk
					local id = tonumber( line:sub(4,6) )
					tiles[id] = parseStringData( line:sub(8,-1), 64 )
					goto ScanNextLine
				end
			end
			
			if scanSprites then
				if line == "-- <SPRITES>" then isSpritesChunk = true; goto ScanNextLine end
				if line == "-- </SPRITES>" then isSpritesChunk = false; goto ScanNextLine end
				
				if isSpritesChunk then
					-- Check if we reached the tiles chunk
					local id = tonumber( line:sub(4,6) )
					sprites[id] = parseStringData( line:sub(8,-1), 64 )
					goto ScanNextLine
				end
			end
			
			if scanPalette then
				-- Check if we reached the palette chunk
				if line == "-- <PALETTE>" then isPaletteChunk = true; goto ScanNextLine end
				if line == "-- </PALETTE>" then isPaletteChunk = false; goto ScanNextLine end
				
				if isPaletteChunk then palettes[#palettes+1] = dataToPalette( line ); goto ScanNextLine end
			end
			
			goto ScanNextLine
			::FileScanEnd::
		end
		
		
		do
			local newSprite = Sprite( 8, 8, ColorMode.INDEXED )
			local bpp = tonumber(data.importBPP)
			local strech = (bpp == 4 and 1) or (bpp == 2 and 2) or (bpp == 1 and 4)
			
			newSprite.properties(pluginKey).import = {
				tiles = scanTiles,
				sprites = scanSprites,
				palette = scanPalette,
				blit = {
					bpp = bpp,
					swapped = false,
					page = 0,
				}
			}
			
			-- Set color mode to indexed
			--app.command.ChangePixelFormat{ format="indexed", dithering="none" }
			
			-- Change width and height depending on the context :
			-- if we scan tile OR sprites, set image dimensions at 128*strech x 128
			-- if we scan tile AND sprites, set image dimensions at 128*strech x 256
			if scanTiles then
				newSprite.width = 128 * strech
				newSprite.height = 128
			end
			if scanSprites then
				if not scanTiles then newSprite.height = 0 end
				newSprite.width = 128 * strech
				newSprite.height = newSprite.height + 128
			end
			
			
			if scanTiles then
				local cel = newSprite.cels[1]
				local yoffset = 0
				cel.image:resize( newSprite.width, newSprite.height )
				
				for id=0, 255 do
					if tiles[id] then
						-- Get pos in vram
						local x = (id % 16) * 8 * strech
						local y = yoffset + (id // 16) * 8
						drawDataChunkTo( cel.image, tiles[id], bpp, strech, x, y )
					end
				end
			end
			
			
			if scanSprites then
				local cel = newSprite.cels[1]
				local yoffset = 128
				cel.image:resize( newSprite.width, newSprite.height )
				
				for id=0, 255 do
					if sprites[id] then
						-- Get pos in vram
						local x = (id % 16) * 8 * strech
						local y = yoffset + (id // 16) * 8
						drawDataChunkTo( cel.image, sprites[id], bpp, strech, x, y )
					end
				end
			end
			
			
			if scanPalette and #palettes > 0 then
				local p = palettes[1]
				newSprite.palettes[1]:resize(16)
				for i=0, #p-1 do
					newSprite.palettes[1]:setColor( i, p:getColor(i) )
				end
			end
			
			
			app.sprite = newSprite
		end
		
		
		do -- continue/close script dialog
			local result = app.alert{
				title = "Importation Done",
				text = "Importation Done !",
				buttons = {"Continue","Close"}
			}
			if result == 1 then goto ScriptStart end
			if result == 2 then goto ScriptEnd end
		end
	end
	
	
	
	if data.generateData then
		local bpp = tonumber(data.exportBPP)
		local str = {}
		
		do
			local invbpp = (bpp == 4 and 2) or (bpp == 2 and 4) or (bpp == 1 and 8)
			local cels = app.sprite.cels
			local raw,buf = {},{}
			
			-- Get each pixel's color index for each frame
			for cid, cel in ipairs(cels) do
				local w, h = cel.image.width, cel.image.height
				local offset = w * h * (cid-1)
				for r=0, h-1 do
					for c=0, w-1 do
						local idx = offset + r * w + c
						raw[idx] = cel.image:getPixel(c,r)
					end
				end
			end
			-- Convert pixels to bytes depending on bbp
			for i=0, #raw do
				local idx = math.floor( i / invbpp )
				local shft = (i % invbpp) * bpp
				if not buf[idx] then buf[idx] = 0 end
				buf[idx] = (buf[idx] + (raw[i] << shft)) % 256
			end
			-- Generate the output string
			for i=0, #buf do
				str[i+1] = string.format("%02x",buf[i])
			end
		end
		
		stateOptions = { data = table.concat(str) }
		currentState = "generatedData"
		goto ScriptStart
	end
	
	::ScriptEnd::
	dlg:close()
end